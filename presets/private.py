from datetime import timedelta

# from commands.cbrf import cbrf_rates
from commands.coin import coin
from commands.rkn import rkn
from commands.roll import roll
from commands.wut import wut

from reactor import Debounce, Sink, Stream
from utils.command import on


class PrivatePreset(Stream):
    def __init__(self):
        super().__init__()

        self >> on('wat', 'wut', 'w') >> Sink(wut)

        self >> on('roll', 'r') >> Sink(roll)

        self >> on('bitcoin', 'buttcoin', 'butt', 'coin') >> Debounce(
            timedelta(minutes=1)) >> Sink(coin)
        # self >> on('rates') >> Debounce(timedelta(minutes=1)) >> Sink(cbrf_rates)
        self >> on('rkn') >> Debounce(timedelta(minutes=1)) >> Sink(rkn)
