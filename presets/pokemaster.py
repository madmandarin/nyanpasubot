from commands.pokemaster.admin import *
from commands.pokemaster.blacklist import *
from commands.pokemaster.poke import *
from commands.pokemaster.subscription import *
from commands.pokemaster.view import *
from reactor import If, Sink, Stream
from utils.admin import CheckChatAdmin
from utils.command import ParsedCommand, on


class PokePreset(Stream):
    def __init__(self):
        super().__init__()

        self >> on('poke:create_channel') >> CheckChatAdmin(loud=True) >> Sink(create_channel)
        self >> on('poke:delete_channel') >> Sink(delete_channel)
        self >> on('poke:create_alias') >> Sink(create_alias)
        self >> on('poke:delete_alias') >> Sink(delete_alias)
        self >> on('poke:create_link') >> Sink(create_link)
        self >> on('poke:delete_link') >> CheckChatAdmin(loud=True) >> Sink(delete_link)

        self >> on('chans', 'ch') >> Sink(list_channels)

        self >> on('sub', 's') >> Sink(subscribe)
        self >> on('unsub', 'u') >> Sink(unsubscribe)
        self >> on('subs', 'ls') >> Sink(list_subscriptions)

        self >> on('mute') >> Sink(mute)
        self >> on('unmute') >> Sink(unmute)

        self >> on('poke', '!') >> Sink(poke)

        async def is_shorthand_poke(command: ParsedCommand):
            return command and command.command.startswith('/')

        self >> If(is_shorthand_poke) >> Sink(poke_shorthand)
