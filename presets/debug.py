from textwrap import dedent

from commands.debug import dump, whoami, allow_chat, allow_poke, disallow_chat, disallow_poke_chat
from reactor import Respond, Sink, Stream
from utils.admin import CheckChatAdmin, CheckRoot
from utils.command import on


class DebugPreset(Stream):
    def __init__(self):
        super().__init__()
        self >> on('d:allow_chat') >> CheckChatAdmin(loud=True) >> Sink(allow_chat)
        self >> on('d:disallow_poke_chat') >> CheckChatAdmin(loud=True) >> Sink(disallow_poke_chat)
        self >> on('d:allow_poke') >> CheckChatAdmin(loud=True) >> Sink(allow_poke)
        self >> on('d:disallow_chat') >> CheckChatAdmin(loud=True) >> Sink(disallow_chat)

        self >> on('d:who') >> Sink(whoami)
        self >> on('d:ump') >> Sink(dump)

        self >> on('d:help') >> Respond(
            dedent(
                """
                    NyanpasuBotBot debug

                    /d:allow_chat - разрешить расширенный список команд для текущего чата
                    /d:allow_poke - разрешить призывание для текущего чата
                    /d:disallow_chat - запретить расширенный список команд для текущего чата

                    /d:who - показать, что бот знает о тебе или другом юзере
                    /d:ump - дампит сообщение, в ответ на которое отправлена эта команда
                """
            ).strip()
        )