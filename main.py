import asyncio

from common.utils import setup_logging
import core


def main():
    setup_logging()

    from conf import sink
    asyncio.run(core.run(sink))


if __name__ == '__main__':
    main()
