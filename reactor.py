import collections
import datetime
import logging
from inspect import Parameter, Signature

from telethon.tl.types import Message


class Context:
    def __init__(self, *args):
        self._data = {}
        self._args = {}
        self.push(self)
        for arg in args:
            self.push(arg)

    def push(self, obj):
        ty = type(obj)

        if ty in self._data:
            raise TypeError(f'Duplicate type in context when adding {obj}!')

        self._data[ty] = obj

    def add_args(self, args):
        self._args = args

    def empty(self, ty):
        if ty in self._data:
            raise TypeError(f'Duplicate type in context when adding {ty}!')

        self._data[ty] = None

    def __contains__(self, ty):
        return ty in self._data

    def __getitem__(self, ty):
        return self._data[ty]

    def get(self, ty):
        return self._data.get(ty)

    def invoke(self, fn):
        kwargs = {}
        for param in Signature.from_callable(fn).parameters.values():
            annotation = param.annotation

            if annotation is Parameter.empty:
                if param.name in self._args:
                    kwargs[param.name] = self._args[param.name]
                else:
                    raise Exception(f"Context parameter {param} of function {fn} does not have a type annotation!")
            elif annotation in self:
                kwargs[param.name] = self[annotation]
            else:
                raise Exception(f"Context parameter of type {annotation} of function {fn} is missing!")

        return fn(**kwargs)

    def __str__(self):
        return f'Context({self._data})'


class Stream:
    def __init__(self):
        self.subscribers = []

    async def recv(self, context):
        await self.send(context)

    async def send(self, context):
        for subscriber in self.subscribers:
            await subscriber.recv(context)

    def __rshift__(self, other):
        self.subscribers.append(other)
        return other


class Log(Stream):
    def __init__(self, name=None):
        super().__init__()

        if name:
            logger_suffix = '({})'.format(name)
        else:
            logger_suffix = ''

        self.logger = logging.getLogger('NyanpasuBot.log_stream' + logger_suffix)

    async def recv(self, context):
        self.logger.debug(f'{context[Message]}')
        await self.send(context)


class If(Stream):
    def __init__(self, cond):
        super().__init__()
        self.cond = cond

    async def recv(self, context):
        if await context.invoke(self.cond):
            await self.send(context)


class Debounce(Stream):
    def __init__(self, timeout):
        super().__init__()
        self.last_seen = collections.defaultdict(lambda: datetime.datetime(year=1970, month=1, day=1))
        self.timeout = timeout

    async def recv(self, context):
        message = context[Message]
        key = message.sender_id, message.chat_id
        now = datetime.datetime.now()
        if now - self.last_seen[key] < self.timeout:
            return
        else:
            self.last_seen[key] = now
            await self.send(context)


class Sink(Stream):
    def __init__(self, callback):
        super().__init__()
        self.callable = callback

    async def recv(self, context):
        await context.invoke(self.callable)


class Respond(Stream):
    def __init__(self, response):
        super().__init__()
        self.response = response

    async def recv(self, context):
        message = context[Message]
        await message.respond(self.response)
