import xdice

from utils.args import args, ArgumentList
from utils.handler import respond_with_yield


@args(
    dice=ArgumentList(help='Кубики')
)
@respond_with_yield
async def roll(dice):
    for die in dice:
        result = xdice.roll(die)
        yield f'Rolled: {result.format()} = {int(result)}'
