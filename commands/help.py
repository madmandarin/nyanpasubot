from telethon.tl.types import Message

HELP_MSG_SMALL = """
NyanpasuBotBot 0.1 "New day, new emoji"

/help - это сообщение

/chans, /ch - список всех каналов

/sub, /s [канал1] [канал2] ... - подписаться на канал
/unsub, /u [канал1] [канал2] ... - отписаться от канала

/subs, /ls - список твоих подписок

/poke, /! [канал1] [канал2] ... - попыщать в канал
/poke, /!, // (без аргументов) - попыщать всех

//[канал] - попыщать в канал (короткая форма)

/mute - выключить уведомления от пыщалки
/unmute - включить уведомления от пыщалки
""".strip()

HELP_MSG_BIG = """
NyanpasuBotBot 0.1 "New day, new emoji"

/help - это сообщение

Полезности:
/wut, /w - исправить раскладку, отправляется в ответ на сообщение (или исправляет последнее полученное ботом сообщение)

Приятности:
/roll, /r - кинуть кубик
/say - говорит то что ты хочешь

Статистики:
/coin - показать курсы биткойнов
/rates - курсы валют ЦБРФ
/rkn - показать число IP-адресов, заблокированных РКН

Команды пыщалки:
/chans, /ch - список всех каналов

/sub, /s [канал1] [канал2] ... - подписаться на канал
/unsub, /u [канал1] [канал2] ... - отписаться от канала

/subs, /ls - список твоих подписок

/poke, /! [канал1] [канал2] ... - попыщать в канал
/poke, /!, // (без аргументов) - попыщать всех

//[канал] - попыщать в канал (короткая форма)

/mute - выключить уведомления от пыщалки
/unmute - включить уведомления от пыщалки

Челлендж:
/checkin - отметиться
/stats - статистика
""".strip()


async def help_small(message: Message):
    orig_message = await message.get_reply_message()
    await message.respond(HELP_MSG_SMALL, reply_to=orig_message)


async def help_big(message: Message):
    orig_message = await message.get_reply_message()
    await message.respond(HELP_MSG_BIG, reply_to=orig_message)
