from telethon.tl.types import Message
from telethon import TelegramClient

from utils.handler import respond_with_return


@respond_with_return
async def rebase(message: Message, tg: TelegramClient):
    if not message.text:
        return

    for item in message.text.split(' '):
        if item.lower() == 'rebase' or item.lower() == 'ребейз':
            await tg.send_file(
                entity=message.chat_id,
                file='assets/keyboard.mp4',
                reply_to=message
            )
