import random

from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import ArgumentList, args
from utils.handler import respond_with_return

# TODO: Отрефакторить на что-то осмысленное
@args(
    help='Попыщать',
    text=ArgumentList(help='Сообщение'),
)
@respond_with_return(parse_mode='markdown')
async def say(message: Message, tg: TelegramClient, text):
    if random.randint(0, 100) > 0:
        try:
            await message.delete()
            return ' '.join(text)
        except:
            return ' '.join(text)
    else:
        return 'Я не буду этого говорить!'
