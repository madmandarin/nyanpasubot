from asyncpg import UniqueViolationError
from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import Argument, Flag, args, ArgumentList
from utils.handler import respond_with_return
from utils.private.models import AllowChats
from utils.private.models import AllowPoke

WHO_TEMPLATE = '''
Юзер:    
```{user}```

Чат: 
```{chat}```
'''.strip()


@args(
    help='Разрешить призывы для текущего чата',
)
@respond_with_return
async def allow_poke(message: Message):
    try:
        chat = await message.get_chat()
        await AllowPoke.create(
            chat_id=chat.id
        )
        return 'Команды разрешены'
    except UniqueViolationError:
        return 'Команды уже разрешены'


@args(
    help='Запретить призывы для текущего чата',
)
@respond_with_return
async def disallow_poke_chat(message: Message):
    tg_chat = await message.get_chat()
    db_chat = await AllowPoke.query.where((AllowChats.chat_id == tg_chat.id)).gino.first()
    if db_chat is None:
        return 'Команды уже запрещены'

    await db_chat.delete()

    return 'Команды запрещены'


@args(
    help='Разрешить расширенный список команд для текущего чата',
)
@respond_with_return
async def allow_chat(message: Message):
    try:
        chat = await message.get_chat()
        await AllowChats.create(
            chat_id=chat.id
        )
        return 'Команды разрешены'
    except UniqueViolationError:
        return 'Команды уже разрешены'


@args(
    help='Запретить расширенный список команд для текущего чата',
)
@respond_with_return
async def disallow_chat(message: Message):
    tg_chat = await message.get_chat()
    db_chat = await AllowChats.query.where((AllowChats.chat_id == tg_chat.id)).gino.first()
    if db_chat is None:
        return 'Команды уже запрещены'

    await db_chat.delete()

    return 'Команды запрещены'


@args(
    user=Argument(help='Пользователь'),
    verbose=Flag(['-v', '--verbose'], help='Показать все, что скрыто')
)
@respond_with_return(parse_mode='markdown')
async def whoami(tg: TelegramClient, message: Message, user, verbose):
    if user:
        if user.isdigit():
            user = int(user)
        try:
            user = await tg.get_entity(user)
        except ValueError:
            return 'Не нашлось такого юзера!'
    else:
        src_message = await message.get_reply_message()

        if src_message is not None:
            message = src_message

        user = await message.get_sender()

    chat = await message.get_chat()

    if verbose:
        return WHO_TEMPLATE.format(user=user.stringify(), chat=chat.stringify())
    else:
        return f'ID юзера: {user.id}; ID чата: {chat.id}'


@respond_with_return(parse_mode='markdown')
async def dump(message: Message):
    orig_message = await message.get_reply_message()

    if not orig_message:
        return f'Эта команда работает только в ответ на сообщение!'

    return f'```{orig_message.stringify()}```'