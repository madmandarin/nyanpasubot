import datetime
from textwrap import dedent

from aiohttp import ClientSession
from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import args
from utils.plotting import figure_to_png, get_figure
from utils.tg import set_uploading_photo


async def get_history(session, currency):
    now = datetime.datetime.now()
    start = now - datetime.timedelta(days=30)

    async with session.get(
            f'https://api.gdax.com/products/{currency}-USD/candles',
            params={
                'granularity': 21600,
                'start': start.strftime('%Y-%m-%d'),
                'end': now.strftime('%Y-%m-%d')
            }
    ) as response:
        data = await response.json()

        xs = []
        ys = []

        for row in data:
            xs.append(datetime.datetime.fromtimestamp(row[0]))
            ys.append(row[3])

        return xs, ys


async def plot_history(session, axes, currency):
    xs, ys = await get_history(session, currency)
    axes.set_title(currency)
    axes.plot(xs, ys)


async def plot(session):
    f = get_figure()
    btc, eth = f.subplots(2, gridspec_kw={'hspace': 0.15})

    await plot_history(session, btc, 'BTC')
    await plot_history(session, eth, 'ETH')

    f.autofmt_xdate()

    return figure_to_png(f)


@args(help='Курсы Bitcoin и Ethereum (Coinbase)')
async def coin(message: Message, http: ClientSession, tg: TelegramClient):
    await set_uploading_photo(tg, message.chat_id)

    async with http.get('https://api.coinbase.com/v2/exchange-rates?currency=USD') as response:
        data = await response.json()

        rates = data['data']['rates']
        caption = dedent(f'''
            1 BTC = {1 / float(rates['BTC']):.2f} USD
            1 ETH = {1 / float(rates['ETH']):.2f} USD
        ''').strip()

        await tg.send_file(
            entity=message.chat_id,
            file=await plot(http),
            caption=caption
        )
