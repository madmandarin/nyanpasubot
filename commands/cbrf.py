import datetime

import zeep.asyncio
from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import args
from utils.plotting import figure_to_png, get_figure
from utils.tg import set_uploading_photo

CLIENT = zeep.Client(
    'https://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL',
    transport=zeep.asyncio.AsyncTransport(loop=None)
)


async def get_currency_codes():
    result = {}
    ev_xml = await CLIENT.service.EnumValutesXML(False)

    for child in ev_xml:
        char_code = child.find('VcharCode')
        code = child.find('Vcode')
        if char_code is not None and code is not None:
            result[char_code.text.strip()] = code.text.strip()

    return result


async def get_history(cb_code, start, end):
    gc_xml = await CLIENT.service.GetCursDynamicXML(start, end, cb_code)

    dates, rates = [], []
    for child in gc_xml:
        date = datetime.datetime.fromisoformat(child.find('CursDate').text)
        dates.append(date)

        rates.append(float(child.find('Vcurs').text))

    return dates, rates


async def plot_history(axes, label, code):
    now = datetime.datetime.now()
    start = now - datetime.timedelta(days=30)

    dates, rates = await get_history(code, start, now)
    axes.plot(dates, rates, label=label)

    return rates[-1]


@args(help='Курсы доллара и евро ЦБРФ')
async def cbrf_rates(message: Message, tg: TelegramClient):
    # FIXME make sure we are running in the current task context
    CLIENT.transport = zeep.asyncio.AsyncTransport(loop=None)

    await set_uploading_photo(tg, message.chat_id)

    f = get_figure()
    ax = f.subplots(1)

    codes = await get_currency_codes()

    usd_rate = await plot_history(ax, 'USD', codes['USD'])
    eur_rate = await plot_history(ax, 'EUR', codes['EUR'])

    f.legend()
    f.autofmt_xdate()

    caption = f'1 USD = {usd_rate:.2f} RUB\n1 EUR = {eur_rate:.2f} RUB'
    await tg.send_file(
        entity=message.chat_id,
        file=figure_to_png(f),
        caption=caption
    )
