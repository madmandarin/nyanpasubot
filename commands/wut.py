from telethon.tl.types import Message

from utils.args import args
from utils.message_cache import MessageCache


def layout_swap(text):
    lat_to_cyr = {
        # lower case
        'q': 'й', 'w': 'ц', 'e': 'у', 'r': 'к', 't': 'е', 'y': 'н', 'u': 'г', 'i': 'ш', 'o': 'щ', 'p': 'з',
        '[': 'х', ']': 'ъ',

        'a': 'ф', 's': 'ы', 'd': 'в', 'f': 'а', 'g': 'п', 'h': 'р', 'j': 'о', 'k': 'л', 'l': 'д', ';': 'ж',
        "'": 'э',

        'z': 'я', 'x': 'ч', 'c': 'с', 'v': 'м', 'b': 'и', 'n': 'т', 'm': 'ь', ',': 'б', '.': 'ю', '/': '.',

        # upper case
        'Q': 'Й', 'W': 'Ц', 'E': 'У', 'R': 'К', 'T': 'Е', 'Y': 'Н', 'U': 'Г', 'I': 'Ш', 'O': 'Щ', 'P': 'З',
        '{': 'Х', '}': 'Ъ',

        'A': 'Ф', 'S': 'Ы', 'D': 'В', 'F': 'А', 'G': 'П', 'H': 'Р', 'J': 'О', 'K': 'Л', 'L': 'Д', ':': 'Ж',
        '"': 'Э',

        'Z': 'Я', 'X': 'Ч', 'C': 'С', 'V': 'М', 'B': 'И', 'N': 'Т', 'M': 'Ь', '<': 'Б', '>': 'Ю', '?': ',',

        # number keys
        '@': '"', '#': '№', '$': ';', '^': ':', '&': '?',
    }

    cyr_to_lat = {
        # lower case
        'й': 'q', 'ц': 'w', 'у': 'e', 'к': 'r', 'е': 't', 'н': 'y', 'г': 'u', 'ш': 'i', 'щ': 'o', 'з': 'p',
        'х': '[', 'ъ': ']',

        'ф': 'a', 'ы': 's', 'в': 'd', 'а': 'f', 'п': 'g', 'р': 'h', 'о': 'j', 'л': 'k', 'д': 'l', 'ж': ';',
        'э': "'",

        'я': 'z', 'ч': 'x', 'с': 'c', 'м': 'v', 'и': 'b', 'т': 'n', 'ь': 'm', 'б': ',', 'ю': '.', '.': '/',

        # upper case
        'Й': 'Q', 'Ц': 'W', 'У': 'E', 'К': 'R', 'Е': 'T', 'Н': 'Y', 'Г': 'U', 'Ш': 'I', 'Щ': 'O', 'З': 'P',
        'Х': '{', 'Ъ': '}',

        'Ф': 'A', 'Ы': 'S', 'В': 'D', 'А': 'F', 'П': 'G', 'Р': 'H', 'О': 'J', 'Л': 'K', 'Д': 'L', 'Ж': ':',
        'Э': '"',

        'Я': 'Z', 'Ч': 'X', 'С': 'C', 'М': 'V', 'И': 'B', 'Т': 'N', 'Ь': 'M', 'Б': '<', 'Ю': '>', '?': '/',

        # number keys
        '"': '@', '№': '#', ';': '$', ':': '^'
    }

    lat_count = 0
    cyr_count = 0
    for char in text:
        if char in lat_to_cyr:
            lat_count += 1
        if char in cyr_to_lat:
            cyr_count += 1

    if cyr_count > lat_count:
        table = cyr_to_lat
    else:
        table = lat_to_cyr

    return text.translate(
        str.maketrans(table)
    )


@args(help='Исправить раскладку сообщения')
async def wut(message: Message, cache: MessageCache):
    orig_message = await message.get_reply_message()

    if orig_message is None:
        orig_message = cache.get(-2)

    if orig_message is None:
        return

    text = orig_message.text

    if not text:
        return

    await message.respond(layout_swap(text), reply_to=orig_message)
