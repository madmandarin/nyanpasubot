from asyncpg import UniqueViolationError
from telethon.tl.types import Message

from utils.args import args
from utils.handler import respond_with_return
from .models import Blacklist

__all__ = [
    'mute', 'unmute'
]


@args(help='Выключить уведомления в этом чате')
@respond_with_return
async def mute(message: Message):
    try:
        await Blacklist.create(
            user_id=message.sender_id,
            chat_id=message.chat_id
        )
        return 'Готово!'
    except UniqueViolationError:
        return 'Уведомления уже выключены!'


@args(help='Включить уведомления в этом чате')
@respond_with_return
async def unmute(message: Message):
    entry = await Blacklist.query.where(
        (Blacklist.user_id == message.sender_id) & (Blacklist.chat_id == message.chat_id)
    ).gino.first()

    if not entry:
        return 'Уведомления уже включены!'

    await entry.delete()

    return 'Готово!'
