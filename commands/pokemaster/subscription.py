from collections import defaultdict

from asyncpg import UniqueViolationError
from sqlalchemy import exists
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from telethon.tl.types import Message

from utils.args import ArgumentList, args
from utils.handler import respond_with_return, respond_with_yield
from common.db.connection import db
from .models import Channel, Subscription
from .utils import resolve_channel

__all__ = ['subscribe', 'unsubscribe', 'list_subscriptions']


def make_subscription_command(callback):
    @args(channels=ArgumentList(required=True, help='Список каналов'))
    @respond_with_yield
    async def _subscription_command_impl(message: Message, channels):
        results = defaultdict(set)

        for channel in channels:
            try:
                result = await callback(
                    user=message.sender_id,
                    channel=await resolve_channel(channel)
                )
                results[result].add(channel)
            except NoResultFound:
                results['Нет такого канала'].add(channel)
            except MultipleResultsFound:
                results['Совпало больше одного канала'].add(channel)

        for k in sorted(results.keys()):
            values = sorted(results[k])
            yield f'{k}: {", ".join(values)}'

    return _subscription_command_impl


@make_subscription_command
async def subscribe(user, channel):
    try:
        await Subscription.create(
            user_id=user,
            channel_id=channel.id
        )
        return 'Подписался'
    except UniqueViolationError:
        return 'Уже подписан'


@make_subscription_command
async def unsubscribe(user, channel):
    sub = await Subscription.query.where(
        (Subscription.user_id == user) & (Subscription.channel_id == channel.id)
    ).gino.first()

    if sub is None:
        return 'Не подписан'

    await sub.delete()

    return 'Отписался'


@args(help='Список твоих подписок')
@respond_with_return
async def list_subscriptions(message: Message):
    async with db.acquire() as connection:
        async with connection.transaction():
            sub_exists = exists(
                Subscription.query.where(
                    (Subscription.user_id == message.sender_id) &
                    (Subscription.channel_id == Channel.id)
                )
            )

            subscriptions = [
                channel.display_name
                async for channel in Channel.query.where(sub_exists).gino.iterate()
            ]

            if not subscriptions:
                return 'Ты ни на что не подписан! Попробуй /sub GW2!'

            return f'Твои подписки: {", ".join(subscriptions)}'
