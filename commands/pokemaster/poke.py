import logging
import urllib.parse

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from telethon import TelegramClient
from telethon.tl.types import Message
from telethon.utils import get_display_name

from utils.args import ArgumentList, Option, args
from utils.command import ParsedCommand
from utils.emoji import *
from utils.handler import respond_with_return
from utils.tg import get_mention_link
from common.config import config
from common.db.connection import db
from .models import Blacklist, Subscription
from .utils import resolve_channel

logger = logging.getLogger('NyanpasuBot.pokemaster')


async def _get_blacklist(chat_id):
    async with db.acquire() as connection:
        async with connection.transaction():
            return {
                bl.user_id
                async for bl in Blacklist.query.where(Blacklist.chat_id == chat_id).gino.iterate()
            }


async def _get_subscribers(channel):
    async with db.acquire() as connection:
        async with connection.transaction():
            return {
                sub.user_id
                async for sub in Subscription.query.where(Subscription.channel_id == channel.id).gino.iterate()
            }


async def _get_entities(tg, user_ids):
    for uid in user_ids:
        try:
            yield await tg.get_entity(uid)
        except ValueError as ex:
            logger.warning(f'Failed to get user {uid}, probably too dead: {ex}')


async def _do_poke(tg, message, targets, targets_text, body_text):
    targets -= await _get_blacklist(message.chat_id)

    if not targets:
        return "Некого звать!"

    targets.discard(message.sender_id)

    if not targets:
        return "Могу позвать только тебя!"

    targets = [
        t
        async for t in _get_entities(tg, targets)
        if not t.bot
    ]

    mentions = "".join(
        get_mention_link(user)
        for user in targets
    )

    sender = await message.get_sender()
    sender_name = get_display_name(sender)

    if body_text:
        first_line_end = f' {body_text}'
    else:
        first_line_end = ''

    return f'{sender_name} @ {targets_text}:{first_line_end}\n\n{mentions}'


def _format_channel(channel):
    if channel.mumble_channel:
        channel_quoted = urllib.parse.quote(channel.mumble_channel)
        channel_url = urllib.parse.urljoin(config.NyanpasuBot.mumble.redirect_url, channel_quoted)
        return f'<a href="{channel_url}">{channel.display_name}</a>'

    return channel.display_name


async def _poke_by_channels(tg, message, channels, text):
    targets = set()
    channels_for_display = []

    for channel in channels:
        try:
            channel = await resolve_channel(channel, message.chat_id)
            targets.update(await _get_subscribers(channel))
            channels_for_display.append(_format_channel(channel))
        except NoResultFound:
            return f'Нет такого канала: {channel}! {ANGRY}'
        except MultipleResultsFound:
            return f'Совпало больше одного канала: {channel}! {ANGRY}'

    # limit to users from this chat
    targets &= {
        user.id async for user in tg.iter_participants(message.chat_id)
    }

    return await _do_poke(
        tg=tg,
        message=message,
        targets=targets,
        targets_text=', '.join(channels_for_display),
        body_text=text
    )


async def _poke_everyone(tg, message, text):
    return await _do_poke(
        tg=tg,
        message=message,
        targets={
            t.id
            async for t in tg.iter_participants(message.chat_id)
        },
        targets_text='все',
        body_text=text
    )


@respond_with_return(parse_mode='html')
async def poke_shorthand(message: Message, tg: TelegramClient, command: ParsedCommand):
    if command.command == '/':
        return await _poke_everyone(tg, message, command.raw_args)

    channels = command.command[1:].split(',')
    return await _poke_by_channels(tg, message, channels, command.raw_args)


@args(
    help='Попыщать',
    channels=ArgumentList(help='Список каналов (или пусто, чтобы попыщать всех)'),
    text=Option(help='Текст для уведомления')
)
@respond_with_return(parse_mode='html')
async def poke(message: Message, tg: TelegramClient, channels, text):
    if not channels:
        return await _poke_everyone(tg, message, text)

    return await _poke_by_channels(tg, message, channels, text)
