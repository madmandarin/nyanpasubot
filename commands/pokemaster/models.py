import sqlalchemy as sa

from common.db.connection import BaseModel


class Channel(BaseModel):
    __tablename__ = 'channel'

    id = sa.Column(sa.Integer(), primary_key=True)
    display_name = sa.Column(sa.Unicode())
    mumble_channel = sa.Column(sa.Unicode(), nullable=True)


class ChannelAlias(BaseModel):
    __tablename__ = 'channel_alias'

    id = sa.Column(sa.Integer(), primary_key=True)
    alias = sa.Column(sa.Unicode(), unique=True)
    channel_id = sa.Column(sa.Integer(), sa.ForeignKey(Channel.id, ondelete='cascade'))


class ChannelLink(BaseModel):
    __tablename__ = 'channel_link'

    id = sa.Column(sa.Integer(), primary_key=True)
    chat_id = sa.Column(sa.BigInteger())
    channel_id = sa.Column(sa.Integer(), sa.ForeignKey(Channel.id, ondelete='cascade'))

    _unique_link_per_chat = sa.Index(
        'unique_link_per_chat',
        'chat_id', 'channel_id',
        unique=True
    )


class Subscription(BaseModel):
    __tablename__ = 'subscription'

    id = sa.Column(sa.Integer(), primary_key=True)
    user_id = sa.Column(sa.BigInteger())
    channel_id = sa.Column(sa.Integer(), sa.ForeignKey(Channel.id, ondelete='cascade'))

    _unique_user_per_channel = sa.Index(
        'unique_user_per_channel',
        'user_id', 'channel_id',
        unique=True
    )


class Blacklist(BaseModel):
    __tablename__ = 'blacklist'

    id = sa.Column(sa.Integer(), primary_key=True)
    user_id = sa.Column(sa.BigInteger())
    chat_id = sa.Column(sa.BigInteger())

    _unique_user_per_chat = sa.Index(
        'unique_user_per_chat',
        'user_id', 'chat_id',
        unique=True
    )
