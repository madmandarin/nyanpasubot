from asyncpg import UniqueViolationError
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import Argument, ArgumentList, args
from utils.handler import respond_with_return
from .models import Channel, ChannelAlias, ChannelLink
from .utils import check_permission, resolve_channel

__all__ = [
    'create_channel',
    'delete_channel',
    'create_alias',
    'delete_alias',
    'create_link',
    'delete_link'
]


@args(
    help='Создать канал',
    channel=Argument(required=True, help='Название канала'),
    mumble_channel=Argument(help='Название канала в Mumble')
)
@respond_with_return
async def create_channel(message: Message, channel, mumble_channel):
    channel = await Channel.create(
        display_name=channel,
        mumble_channel=mumble_channel
    )

    await ChannelLink.create(
        chat_id=message.chat_id,
        channel_id=channel.id
    )

    return f'Готово!'


@args(
    help='Создать алиас канала',
    channel=Argument(required=True, help='Имя или ID канала'),
    aliases=ArgumentList(required=True, help='Алиасы')
)
@respond_with_return
async def create_alias(tg: TelegramClient, message: Message, channel, aliases):
    try:
        channel = await resolve_channel(channel)
    except NoResultFound:
        return 'Нет такого канала!'
    except MultipleResultsFound:
        return 'Совпало больше одного канала!'

    if not await check_permission(tg, message.sender_id, channel.id):
        return 'Твое кунг-фу не достаточно сильное!'

    fails = []
    for alias in aliases:
        if alias == channel.display_name:
            # avoid duplicates
            continue

        try:
            await ChannelAlias.create(
                alias=alias,
                channel_id=channel.id
            )
        except UniqueViolationError:
            fails.append(alias)

    if fails:
        suffix = f' Уже есть алиасы: {", ".join(fails)}'
    else:
        suffix = ''

    return f'Готово!{suffix}'


@args(
    help='Привязать канал к этому чату',
    channel=Argument(required=True, help='Имя или ID канала')
)
@respond_with_return
async def create_link(tg: TelegramClient, message: Message, channel):
    try:
        channel = await resolve_channel(channel)
    except NoResultFound:
        return 'Нет такого канала!'
    except MultipleResultsFound:
        return 'Совпало больше одного канала!'

    if not await check_permission(tg, message.sender_id, channel.id):
        return 'Твое кунг-фу не достаточно сильное!'

    try:
        await ChannelLink.create(
            chat_id=message.chat_id,
            channel_id=channel.id
        )
        return 'Готово!'
    except UniqueViolationError:
        return 'Этот канал уже привязан!'


@args(
    help='Удалить канал',
    channel=Argument(required=True, help='Имя, алиас или ID канала')
)
@respond_with_return
async def delete_channel(tg: TelegramClient, message: Message, channel):
    try:
        channel = await resolve_channel(channel)
    except NoResultFound:
        return 'Нет такого канала!'
    except MultipleResultsFound:
        return 'Совпало больше одного канала!'

    if not await check_permission(tg, message.sender_id, channel.id):
        return 'Твое кунг-фу не достаточно сильное!'

    await channel.delete()

    return 'Готово!'


@args(
    help='Удалить алиас канала',
    alias=Argument(required=True, help='Алиас')
)
@respond_with_return
async def delete_alias(tg: TelegramClient, message: Message, alias):
    alias = await ChannelAlias.load(channel=Channel).query.where(ChannelAlias.alias == alias).gino.first()

    if not alias:
        return 'Нет такого алиаса!'

    if not await check_permission(tg, message.sender_id, alias.channel.id):
        return 'Твое кунг-фу не достаточно сильное!'

    await alias.delete()

    return 'Готово!'


@args(
    help='Отвязать канал от этого чата',
    channel=Argument(required=True, help='Имя, алиас или ID канала')
)
@respond_with_return
async def delete_link(message: Message, channel):
    try:
        channel = await resolve_channel(channel)
    except NoResultFound:
        return 'Нет такого канала!'
    except MultipleResultsFound:
        return 'Совпало больше одного канала!'

    link = await ChannelLink.query.where(
        (ChannelLink.chat_id == message.chat_id) & (ChannelLink.channel_id == channel.id)
    ).gino.first()

    await link.delete()

    return 'Готово!'
