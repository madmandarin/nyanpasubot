from sqlalchemy import func
from telethon import TelegramClient
from telethon.tl.types import Message
from telethon.utils import get_display_name

from commands.pokemaster.models import Channel, ChannelAlias, ChannelLink, Subscription
from utils.args import Flag, args
from utils.handler import respond_with_yield
from common.db.connection import db

__all__ = ['list_channels']


@args(
    help='Список каналов в этом чате',
    show_users=Flag(['-u', '--users'], help='Показать список пользователей'),
    show_ids=Flag(['-i', '--ids'], help='Показать ID каналов'),
    show_all=Flag(['-a', '--all'], help='Показать все каналы')
)
@respond_with_yield(parse_mode='html', silent=True)
async def list_channels(tg: TelegramClient, message: Message, show_users, show_ids, show_all):
    found_any = False

    async with db.acquire() as connection:
        async with connection.transaction():
            annotated = Channel.outerjoin(ChannelAlias).outerjoin(ChannelLink).outerjoin(Subscription).select()

            if not show_all:
                annotated = annotated.where(ChannelLink.chat_id == message.chat_id)

            aggregations = [
                Channel.id,
                Channel.display_name,
                func.array_remove(func.array_agg(func.distinct(ChannelAlias.alias)), None),
                func.array_remove(func.array_agg(func.distinct(Subscription.user_id)), None)
            ]
            grouped = annotated.group_by(Channel.id).with_only_columns(aggregations)

            async for c_id, name, aliases, users in grouped.gino.iterate():
                found_any = True

                parts = ['-']

                if show_ids:
                    parts.append(f'(#{c_id})')

                parts.append(f'<b>{name}</b>')

                if aliases:
                    parts.append(f'[{", ".join(aliases)}]')

                yield ' '.join(parts)

                if show_users:
                    for user in users:
                        user = await tg.get_entity(user)
                        yield f'  - {get_display_name(user)}'

    if not found_any:
        yield 'Нет каналов!'
