from sqlalchemy import exists
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from telethon import TelegramClient
from telethon.tl.types import ChannelParticipantsAdmins

from common.db.connection import db
from .models import Channel, ChannelAlias, ChannelLink


async def one(queryset):
    result = None
    async for row in queryset.gino.iterate():
        if result is not None:
            raise MultipleResultsFound

        result = row

    if result is None:
        raise NoResultFound

    return result


async def resolve_channel(query, chat_id=None):
    async with db.acquire() as connection:
        async with connection.transaction():
            channels = Channel.query

            if chat_id is not None:
                channels = channels.where(
                    exists(
                        ChannelLink.query.where(
                            (ChannelLink.chat_id == chat_id) &
                            (ChannelLink.channel_id == Channel.id)
                        )
                    )
                )

            alias_exists = exists(
                ChannelAlias.query.where(
                    (ChannelAlias.channel_id == Channel.id) &
                    (ChannelAlias.alias == query)
                )
            )
            filters = (Channel.display_name == query) | alias_exists
            if query.isdigit():
                filters |= (Channel.id == int(query))

            return await one(channels.where(filters))


async def check_permission(tg: TelegramClient, user_id, channel_id):
    async with db.acquire() as connection:
        async with connection.transaction():
            links = ChannelLink.query.where(ChannelLink.channel_id == channel_id).gino.iterate()
            async for link in links:
                admins = tg.iter_participants(
                    entity=link.chat_id,
                    filter=ChannelParticipantsAdmins()
                )
                admin_ids = {u.id async for u in admins}
                if user_id not in admin_ids:
                    return False

            return True
