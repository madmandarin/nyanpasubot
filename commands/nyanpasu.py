from telethon.tl.types import Message
from telethon import TelegramClient


async def nyanpasu(message: Message, tg: TelegramClient):
    await tg.send_file(
        entity=message.chat_id,
        file='assets/nyanpasu1.mp4',
        reply_to=message
    )
