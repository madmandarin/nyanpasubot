import random
import unicodedata
from collections import defaultdict, deque

from telethon.tl.types import Message

from utils.handler import respond_with_return

confusables_x = 'XΧХ᙭ᚷⅩ╳ⲬⵝꓫꞳ𐊐𐊴𐌗𐌢𝐗𝑋𝑿𝒳𝓧𝔛𝕏𝖃𝖷𝗫𝘟𝙓𝚇𝚾𝛸𝜲𝝬𝞦Ｘx×хᕁᕽ᙮ⅹ⤫⤬⨯𝐱𝑥𝒙𝓍𝔁𝔵𝕩𝖝𝗑𝘅𝘹𝙭𝚡ｘ𝟀❌'
confusables_e = 'EΕЕᎬℰ⋿ⴹꓰ𐊆𝐄𝐸𝑬𝓔𝔈𝔼𝕰𝖤𝗘𝘌𝙀𝙴𝚬𝛦𝜠𝝚𝞔Ｅeеҽ℮ℯⅇꬲ𝐞𝑒𝒆𝓮𝔢𝕖𝖊𝖾𝗲𝘦𝙚𝚎ｅ'
confusables_h = 'hһհᏂℎ𝐡𝒉𝒽𝓱𝔥𝕙𝖍𝗁𝗵𝘩𝙝𝚑ｈHΗНᎻᕼℋℌℍⲎꓧ𐋏𝐇𝐻𝑯𝓗𝕳𝖧𝗛𝘏𝙃𝙷𝚮𝛨𝜢𝝜𝞖Ｈ'
confusables_y = 'yɣʏγуүყᶌỿℽꭚ𑣜𝐲𝑦𝒚𝓎𝔂𝔶𝕪𝖞𝗒𝘆𝘺𝙮𝚢𝛄𝛾𝜸𝝲𝞬ｙYΥϒУҮᎩᎽⲨꓬ𐊲𝐘𝑌𝒀𝒴𝓨𝔜𝕐𝖄𝖸𝗬𝘠𝙔𝚈𝚼𝛶𝜰𝝪𝞤Ｙ'

LAST_MESSAGES = defaultdict(lambda: deque(maxlen=10))


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) not in {'Mn', 'Cf', 'Cc', 'Co'})


def check_heh(text):

    for item in text.split(' '):
        if item.lower() == 'хех':
            return 'ххнуе'

        word = strip_accents(item.lower())

        if len(item.lower()) >=3 and word[0] in 'хx' and word[1] in 'еe' and word[2] in 'xх':
            return 'xxhye'

        if len(item.lower()) >=3 and word[0] in confusables_x and word[1] in confusables_e and word[2] in confusables_x:
            return (
                    random.choice(confusables_x) + random.choice(confusables_x) +
                    random.choice(confusables_h) + random.choice(confusables_y) +
                    random.choice(confusables_e)
            )

    return None


@respond_with_return
async def heh(message: Message):
    if not message.text:
        return

    result = check_heh(message.text)

    queue = LAST_MESSAGES[message.sender_id]
    queue.append(result is not None)

    if len(queue) > 5 and result:
        frustration_chance = sum(queue) / len(queue) - 0.2

        if random.random() < frustration_chance:
            queue.clear()
            return 'ХУЕХ >____>'

    return result
