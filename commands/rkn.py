import datetime

import math
import pytz
from aiohttp import ClientSession
from matplotlib import dates as mdates, ticker
from telethon import TelegramClient
from telethon.tl.types import Message

from utils.args import args
from utils.plotting import figure_to_png, get_figure
from utils.tg import set_uploading_photo


async def get_history(session, interval):
    async with session.get(f'https://usher2.club/{interval}_ipblock.json') as response:
        data = await response.json()

        xs = []
        ys = []

        for row in data:
            xs.append(datetime.datetime.fromtimestamp(row['x'], tz=pytz.utc))
            ys.append(row['y'])

        return xs, ys


def plot(d1_xs, d1_ys, d15_xs, d15_ys):
    f = get_figure()
    d1, d15 = f.subplots(2, gridspec_kw={'hspace': 0.45})

    @ticker.FuncFormatter
    def k_formatter(y, _):
        if y == 0:
            return 0
        ks = math.floor(math.log(abs(y), 1000))
        return f'{y / (1000 ** ks):.2f}' + 'к' * ks

    d1.set_title('Число заблокированных IP-адресов за сутки')

    d1.xaxis.set_major_formatter(
        mdates.DateFormatter(
            '%H:%M',
            tz=pytz.timezone('Europe/Moscow')
        )
    )
    d1.yaxis.set_major_formatter(k_formatter)
    d1.grid(True)

    d1.plot(d1_xs, d1_ys)

    d15.set_title('Число заблокированных IP-адресов за 15 дней')

    d15.yaxis.set_major_formatter(k_formatter)
    d15.grid(True)

    d15.plot(d15_xs, d15_ys)

    for label in d1.get_xticklabels():
        label.set_ha('right')
        label.set_rotation(30)

    for label in d15.get_xticklabels():
        label.set_ha('right')
        label.set_rotation(30)

    return figure_to_png(f)


@args(help='Количество IP-адресов в реестре блокировок РКН')
async def rkn(message: Message, http: ClientSession, tg: TelegramClient):
    await set_uploading_photo(tg, message.chat_id)

    d1_xs, d1_ys = await get_history(http, 'd1')
    d15_xs, d15_ys = await get_history(http, 'd15')

    image = plot(d1_xs, d1_ys, d15_xs, d15_ys)
    caption = f'Великий файрвол завершен на <b>{d1_ys[-1]:_}</b> IP-адресов'.replace('_', ' ').strip()
    await tg.send_file(
        entity=message.chat_id,
        file=image,
        caption=caption,
        parse_mode='html'
    )
