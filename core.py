import itertools
import logging
import os.path
import sys
import traceback

import aiohttp
from telethon import events

from common.config import config
from common.db import connection
from reactor import Context

logger = logging.getLogger('NyanpasuBot.tg')


def format_traceback():
    src_dir = os.path.join(os.path.abspath(__file__), '..')

    exc_type, value, tb = sys.exc_info()
    traces = traceback.extract_tb(tb)

    def is_stream(trace):
        return (
                os.path.basename(trace.filename) == 'reactor.py'
                or trace.name == 'recv'
        )

    for key, traces in itertools.groupby(traces, is_stream):
        if key:
            yield '...'
            continue

        for filename, line, fname, text in traces:
            try:
                filename = os.path.relpath(filename, src_dir)
            except ValueError:
                pass

            yield f'{filename}:{line}, in {fname}: {text}'

    yield f'{exc_type.__qualname__}: {value}'


async def run(sink):
    logger.info('Starting bot...')

    logger.info('Initializing HTTP...')
    http = aiohttp.ClientSession()

    logger.info('Initializing Telegram...')
    tg = config.get_tg_client()

    async def handle_message(msg):
        # noinspection PyBroadException
        try:
            await sink.recv(
                context=Context(tg, http, msg.message)
            )
        except Exception:
            logger.exception(f'Downstream exception, responding with traceback')

            tb = '\n'.join(format_traceback())

            await msg.respond(f'```{tb}```', parse_mode='markdown')

    tg.add_event_handler(handle_message, events.NewMessage)

    logger.info('Connecting to database...')
    await connection.init()

    logger.info('Connecting to Telegram...')
    await tg.connect()

    logger.info('Signing in...')
    await tg.sign_in(bot_token=config.tg.token)

    logger.info('Ready!')
    await tg.disconnected
