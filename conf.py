from telethon.tl.types import Message
from commands.nyanpasu import nyanpasu
from commands.say import say
from commands.help import help_big, help_small
from commands.heh import heh
from commands.rebase import rebase
from presets.private import PrivatePreset
from presets.debug import DebugPreset
from presets.pokemaster import PokePreset
from reactor import Sink, Stream, If
from utils.command import LabelCommand, chance, on
from utils.message_cache import AddMessageCache
from utils.tg import AutoCacheChats
from utils.private.private import CheckChatAllow, CheckChatPoke

sink = Stream()


async def has_sender(message: Message):
    return message.sender_id is not None


source = sink >> \
         If(has_sender) >> \
         LabelCommand() >> \
         AddMessageCache() >> \
         AutoCacheChats()

source >> DebugPreset()
source >> CheckChatPoke(nyanpasu)  >> PokePreset()
source >> CheckChatAllow(nyanpasu) >> PrivatePreset()

source >> on('say') >> Sink(say)
# source >> chance(0) >> Sink(nyanpasu)
source >> on('nyanpasu') >> Sink(nyanpasu)
source >> on('help') >> CheckChatAllow(help_small) >> Sink(help_big)

source >> Sink(heh)
source >> Sink(rebase)
