import pathlib
from dataclasses import MISSING, fields, is_dataclass

import toml


def load(cls):
    config_path = pathlib.Path(__file__).parent / '..' / '..' / 'config.toml'
    data = toml.load(config_path.open())
    return fill(cls, data)


def fill(cls, data):
    assert is_dataclass(cls)

    kwargs = {}
    for field in fields(cls):
        if is_dataclass(field.type) and field.name in data:
            kwargs[field.name] = fill(field.type, data[field.name])
        elif field.name in data:
            kwargs[field.name] = field.type(data[field.name])
        elif field.default != MISSING:
            kwargs[field.name] = field.default
        elif field.default_factory != MISSING:
            kwargs[field.name] = field.default_factory()

    return cls(**kwargs)


def build_skeleton(cls):
    assert is_dataclass(cls)

    result = {}
    for field in fields(cls):
        if is_dataclass(field.type):
            result[field.name] = build_skeleton(field.type)
        else:
            result[field.name] = field.type()

    return result
