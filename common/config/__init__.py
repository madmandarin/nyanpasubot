from dataclasses import dataclass, field

from telethon import TelegramClient
from telethon.sessions import SQLiteSession

from . import loader


@dataclass
class DbConfig:
    url: str


@dataclass
class HostConfig:
    host: str
    port: int


@dataclass
class TgProxyConfig(HostConfig):
    username: str
    password: str


@dataclass
class TgChatsConfig:
    announcements: int
    monitoring: int


@dataclass
class TgConfig:
    api_id: int
    api_hash: str
    token: str
    admins: list
    chats: TgChatsConfig
    ipv6: bool = field(default=True)
    proxy: TgProxyConfig = field(default=None)


@dataclass
class LogsConfig:
    chats: dict


@dataclass
class Config:
    database: DbConfig
    tg: TgConfig
    logs: LogsConfig = field(default=None)

    def get_tg_client(self, session=None, **kwargs):
        if self.tg.proxy:
            import socks

            proxy = (
                socks.SOCKS5,
                self.tg.proxy.host,
                self.tg.proxy.port,
                True,
                self.tg.proxy.username,
                self.tg.proxy.password
            )
            ipv6 = False
        else:
            proxy = None
            ipv6 = self.tg.ipv6

        if session is None:
            session = SQLiteSession()

        return TelegramClient(
            session=session,
            api_id=self.tg.api_id,
            api_hash=self.tg.api_hash,
            use_ipv6=ipv6,
            proxy=proxy,
            **kwargs
        )


config = loader.load(Config)
