import gino

from common.config import config

db = gino.Gino()


class BaseModel(db.Model):
    pass


async def init():
    await db.set_bind(config.database.url)
    await db.gino.create_all()
