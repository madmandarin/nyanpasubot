import logging

import coloredlogs
import requests

from common.config import config


def setup_logging(**kwargs):
    coloredlogs.install(
        level=logging.DEBUG,
        fmt='%(asctime)s %(name)40s %(levelname)10s %(message)s',
        **kwargs
    )

    noisy_loggers = {
        'asyncio',

        'urllib3',

        'gino.engine._SAEngine',

        'telethon.client.downloads',
        'telethon.extensions.messagepacker',
        'telethon.network.mtprotosender',

        'zeep.asyncio.transport',
        'zeep.xsd.schema',
        'zeep.wsdl.bindings.soap',
        'zeep.wsdl.wsdl',

        'matplotlib',
        'matplotlib.font_manager',
        'PIL.PngImagePlugin',

        'pymorphy2.opencorpora_dict.wrapper'
    }
    for logger in noisy_loggers:
        logging.getLogger(logger).setLevel(logging.WARNING)


def send_tg_message(chat_id, text, parse_mode=None):
    requests.get(
        f'https://api.telegram.org/bot{config.tg.token}/sendMessage',
        params={
            'chat_id': chat_id,
            'text': text,
            'parse_mode': parse_mode
        }
    )
