import logging
import random
from dataclasses import dataclass

from telethon import TelegramClient
from telethon.tl.types import Message

from reactor import If, Stream

logger = logging.getLogger('NyanpasuBot.command')


@dataclass
class ParsedCommand:
    command: str
    raw_args: str


async def parse_command(message: Message, tg: TelegramClient):
    if not message.text:
        return

    text = message.text
    if not text.startswith('/'):
        return

    parts = text.split(maxsplit=1)
    if len(parts) == 1:
        command, args = parts[0], None
    else:
        command, args = parts

    command = command[1:]  # remove /

    if "@" in command:
        command, bot_name = command.split("@")
        if bot_name != (await tg.get_me()).username:
            logger.warning(f'Saw foreign bot command: {command}@{bot_name}')
            return

    return ParsedCommand(command, args)


class LabelCommand(Stream):
    async def recv(self, context):
        parsed = await context.invoke(parse_command)
        if parsed:
            context.push(parsed)
        else:
            context.empty(ParsedCommand)

        await self.send(context)


def on(*commands):
    async def predicate(command: ParsedCommand):
        return command and command.command in commands

    return If(predicate)


def chance(chance_value):
    async def predicate():
        return chance_value >= random.randint(0, 100)
    return If(predicate)
