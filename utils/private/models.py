import sqlalchemy as sa

from common.db.connection import BaseModel


class AllowChats(BaseModel):
    __tablename__ = 'allow_chats'

    id = sa.Column(sa.Integer(), primary_key=True)
    chat_id = sa.Column(sa.BigInteger(), unique=True)


class AllowPoke(BaseModel):
    __tablename__ = 'allow_poke'

    id = sa.Column(sa.Integer(), primary_key=True)
    chat_id = sa.Column(sa.BigInteger(), unique=True)
