from abc import ABCMeta, abstractmethod
from telethon.tl.types import Message

from utils.private.models import AllowChats, AllowPoke
from common.db.connection import db
from reactor import Stream


class CheckChatAllow(Stream, metaclass=ABCMeta):
    def __init__(self, callback):
        super().__init__()
        self.callable = callback

    async def __get_allow_chats(self):
        agg = AllowChats.select().with_only_columns([
            AllowChats.chat_id
        ])

        async with db.acquire() as connection:
            async with connection.transaction():
                return [
                    row.chat_id
                    async for row in agg.gino.iterate()
                ]

    async def recv(self, context):
        chat = await context[Message].get_chat()
        allow_chats = await self.__get_allow_chats()
        if chat.id in allow_chats:
            await self.send(context)


class CheckChatPoke(Stream, metaclass=ABCMeta):
    def __init__(self, callback):
        super().__init__()
        self.callable = callback

    async def __get_allow_chats(self):
        agg = AllowPoke.select().with_only_columns([
            AllowPoke.chat_id
        ])

        async with db.acquire() as connection:
            async with connection.transaction():
                return [
                    row.chat_id
                    async for row in agg.gino.iterate()
                ]

    async def recv(self, context):
        chat = await context[Message].get_chat()
        allow_chats = await self.__get_allow_chats()
        if chat.id in allow_chats:
            await self.send(context)

