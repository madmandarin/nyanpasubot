import logging

from telethon import TelegramClient
from telethon.tl.functions.messages import SetTypingRequest
from telethon.tl.types import Message, SendMessageUploadPhotoAction

from reactor import Stream

logger = logging.getLogger('NyanpasuBot.utils.tg')


def get_mention_link(user, text='.'):
    return f'<a href="tg://user?id={user.id}">{text}</a>'


async def set_uploading_photo(tg, chat):
    await tg(
        SetTypingRequest(
            peer=chat,
            action=SendMessageUploadPhotoAction(progress=0)
        )
    )


class AutoCacheChats(Stream):
    def __init__(self):
        super().__init__()
        self.already_cached = set()

    async def recv(self, context):
        message = context[Message]
        tg = context[TelegramClient]

        if message.chat_id not in self.already_cached:
            if await precache_chat(tg, message.chat_id):
                self.already_cached.add(message.chat_id)

        await self.send(context)


async def precache_chat(tg, chat_id):
    try:
        async for _ in tg.iter_participants(chat_id):
            pass
        return True
    except ValueError:
        logger.exception(f'Failed to precache chat {chat_id}!')
        return False
