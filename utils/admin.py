from abc import ABCMeta, abstractmethod

from telethon import TelegramClient
from telethon.tl.types import ChannelParticipantsAdmins, Message

from reactor import Stream
from utils.emoji import *
from common.config import config


class CheckPermissionBase(Stream, metaclass=ABCMeta):
    def __init__(self, loud=False):
        super().__init__()
        self.loud = loud

    @abstractmethod
    async def predicate(self, context):
        pass

    async def recv(self, context):
        if await self.predicate(context):
            await self.send(context)
        elif self.loud:
            await context[Message].respond(f'Это только для админов! {ANGRY}')


class CheckRoot(CheckPermissionBase):
    async def predicate(self, context):
        return context[Message].sender_id in config.tg.admins


class CheckChatAdmin(CheckPermissionBase):
    async def predicate(self, context):
        message = context[Message]
        tg = context[TelegramClient]

        async for user in tg.iter_participants(
                entity=message.chat_id,
                filter=ChannelParticipantsAdmins()
        ):
            if user.id == message.sender_id:
                return True
        return False
