import dateutil.relativedelta as rd
import pymorphy2

morph = pymorphy2.MorphAnalyzer()
day_word = morph.parse('день')[0]
month_word = morph.parse('месяц')[0]
year_word = morph.parse('год')[0]


def format_days(num_days):
    word = day_word.make_agree_with_number(num_days).word
    return f'{num_days} {word}'


def format_months(num_months):
    word = month_word.make_agree_with_number(num_months).word
    return f'{num_months} {word}'


def format_years(num_years):
    word = year_word.make_agree_with_number(num_years).word
    return f'{num_years} {word}'


def format_timedelta(end_date, start_date):
    if start_date > end_date:
        sign = '-'
        start_date, end_date = end_date, start_date
    else:
        sign = ''

    delta = rd.relativedelta(end_date, start_date)

    parts = []
    if delta.years:
        parts.append(format_years(delta.years))

    if delta.months:
        parts.append(format_months(delta.months))

    if delta.days:
        parts.append(format_days(delta.days))

    if not parts:
        return '0'

    return sign + ' '.join(parts)
