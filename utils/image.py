import io


def image_to_jpeg(image):
    with io.BytesIO() as out:
        image.save(out, format='jpeg')
        return out.getvalue()
