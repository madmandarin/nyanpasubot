from abc import ABC, abstractmethod

import click.parser
from lark import Lark, Transformer, v_args
from lark.exceptions import LarkError
from telethon.tl.types import Message

from reactor import Context
from utils.handler import magic_decorator
from .command import ParsedCommand


class ArgsTransformer(Transformer):
    @staticmethod
    def space(_):
        return None

    @v_args(inline=True)
    def quoted(self, string):
        return string[1:-1]

    @v_args(inline=True)
    def literal(self, string):
        return string[:]

    @staticmethod
    def arg(children):
        return ''.join(c[:] for c in children)

    @staticmethod
    def args(children):
        return [c for c in children if c]


TEXT_PARSER = Lark(
    grammar=r"""
        %import common.WS
        space: WS

        quoted: /".*?(?<!\\)(\\\\)*?"/i
        literal: /[^\s\"]+/i
        arg: (quoted | literal)+
        args: (arg [space])+
    """,
    start='args',
    parser='lalr',
    transformer=ArgsTransformer()
)

EMDASH = '\u2014'


class CliError(Exception):
    def __init__(self, message):
        self.message = message


def identity(x):
    return x


class Parameter(ABC):
    # noinspection PyShadowingBuiltins
    def __init__(self, required=False, type=None, help=None):
        self.required = required

        if type is None:
            type = identity

        self.type = type
        self.help = help

    @abstractmethod
    def add_to_parser(self, parser, name):
        raise NotImplemented

    def check_required(self, value):
        if self.required and (value is None or len(value) == 0):
            raise CliError('Не указан обязательный аргумент!')

    def try_convert(self, value):
        if value is None:
            return None

        try:
            return self.type(value)
        except Exception as ex:
            raise CliError(f"Не знаю, как преобразовать значение {value}: {ex}")

    def try_convert_list(self, values):
        if values is None:
            return []

        return [self.try_convert(value) for value in values]

    def build_help(self, name):
        if self.help:
            help_suffix = f': {self.help}'
        else:
            help_suffix = ''

        return f'**{name}**{help_suffix}'


class Argument(Parameter):
    # noinspection PyShadowingBuiltins
    def __init__(self, *, required=False, type=None, help=None):
        super().__init__(required, type, help)

    def add_to_parser(self, parser, name):
        parser.add_argument(name, nargs=1)

    def process_value(self, value):
        self.check_required(value)
        return self.try_convert(value)

    def build_help(self, name):
        if not self.required:
            name = f'[{name}]'
        return super().build_help(name)


class ArgumentList(Argument):
    # noinspection PyShadowingBuiltins
    def __init__(self, *, required=False, type=None, help=None):
        super().__init__(required=required, type=type, help=help)

    def add_to_parser(self, parser, name):
        parser.add_argument(name, nargs=-1)

    def process_value(self, values):
        self.check_required(values)
        return self.try_convert_list(values)

    def build_help(self, name):
        return super().build_help(name + '...')


class Option(Parameter):
    # noinspection PyShadowingBuiltins
    def __init__(self, aliases=None, *, required=False, type=None, help=None):
        super().__init__(required, type, help)

        if aliases is None:
            aliases = []
        self.aliases = aliases

    def get_option_names(self, name):
        return [f'--{name}'] + self.aliases

    def add_to_parser(self, parser, name):
        parser.add_option(self.get_option_names(name), name, action='store')

    def process_value(self, value):
        self.check_required(value)
        return self.try_convert(value)

    def build_help(self, name):
        return super().build_help(', '.join(f'{opt}=VALUE' for opt in self.get_option_names(name)))


class Flag(Option):
    # noinspection PyShadowingBuiltins
    def __init__(self, aliases=None, help=None):
        super().__init__(aliases, type=bool, help=help)

    def add_to_parser(self, parser, name):
        parser.add_option(self.get_option_names(name), name, action='store_const', const=True)

    def process_value(self, value):
        return bool(super().process_value(value))

    def build_help(self, name):
        return Parameter.build_help(self, ', '.join(self.get_option_names(name)))


class ListOption(Option):
    # noinspection PyShadowingBuiltins
    def __init__(self, aliases=None, *, required=False, type=None, help=None):
        super().__init__(aliases, required=required, type=type, help=help)

    def add_to_parser(self, parser, name):
        parser.add_option(self.get_option_names(name), name, action='append')

    def process_value(self, values):
        self.check_required(values)
        return self.try_convert_list(values)


class Parser:
    def __init__(self, help_msg=None, **kwargs):
        self.help_msg = help_msg
        self.parameters = kwargs
        self.parameters['help'] = Flag(['-h'], help='Вывести это сообщение')
        self.parser = click.parser.OptionParser()

        for name, param in self.parameters.items():
            param.add_to_parser(self.parser, name)

    # noinspection PyShadowingNames
    def parse(self, args):
        try:
            parsed, extra, _ = self.parser.parse_args(args)
        except click.BadOptionUsage as ex:
            raise CliError(f'{ex.option_name}: {ex.message}')
        except click.NoSuchOption as ex:
            raise CliError(f'Нет такого параметра: {ex.option_name}')

        if parsed.get('help'):
            return {'help': True}

        if extra:
            raise CliError(f'Лишние аргументы: {extra}')

        values = {}
        for name, param in self.parameters.items():
            value = parsed.get(name)

            try:
                values[name] = param.process_value(value)
            except CliError as ex:
                raise CliError(f'{name}: {ex.message}')

        return values

    def build_help(self):
        positional, options, other = [], [], []
        for name, param in self.parameters.items():
            msg = '  \u2022 ' + param.build_help(name)
            if isinstance(param, Argument):
                positional.append(msg)
            elif isinstance(param, Option):
                options.append(msg)
            else:
                other.append(msg)

        lines = []

        if self.help_msg:
            lines.append(self.help_msg)

        if positional:
            lines.append('**Позиционные аргументы:**')
            lines.extend(positional)

        if options:
            lines.append('**Флаги:**')
            lines.extend(options)

        if other:
            lines.append('**Другое:**')
            lines.extend(other)

        return '\n'.join(lines)


@magic_decorator
def args(fn, **arguments):
    if 'help' in arguments:
        help_msg = arguments['help']
        del arguments['help']
    else:
        help_msg = None

    arg_parser = Parser(help_msg, **arguments)

    async def wrapper(message: Message, cmd: ParsedCommand, context: Context):
        try:
            if cmd.raw_args:
                raw_args = [
                    '--' + item[1:] if item.startswith(EMDASH) else item
                    for item in TEXT_PARSER.parse(cmd.raw_args)
                ]
            else:
                raw_args = []

            values = arg_parser.parse(raw_args)

            if values.get('help'):
                await message.respond(arg_parser.build_help())
                return

            context.add_args(values)
            return await context.invoke(fn)
        except LarkError as ex:
            await message.respond(f'Ошибка при разборе команды:\n```{ex.args[0]}```', parse_mode='markdown')
        except CliError as ex:
            await message.respond(ex.message + '\n\n' + arg_parser.build_help(), parse_mode='markdown')

    return wrapper
