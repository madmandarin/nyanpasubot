from collections import defaultdict, deque

from telethon.tl.types import Message

from reactor import Stream


class MessageCache:
    def __init__(self, message, cache):
        self.message = message
        self.cache = cache

    def get(self, idx, chat_id=None):
        if chat_id is None:
            chat_id = self.message.chat_id

        try:
            if chat_id in self.cache.caches:
                return self.cache.caches[chat_id][idx]
        except IndexError:
            pass

        return None


class AddMessageCache(Stream):
    def __init__(self, size=5):
        super().__init__()
        self.caches = defaultdict(lambda: deque(maxlen=size))

    async def recv(self, context):
        message = context[Message]
        self.caches[message.chat_id].append(message)
        context.push(MessageCache(message, self))
        await self.send(context)
