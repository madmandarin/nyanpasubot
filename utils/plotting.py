import io

from matplotlib import figure
from matplotlib.backends.backend_agg import FigureCanvasAgg


def get_figure():
    f = figure.Figure()
    f.set_canvas(FigureCanvasAgg(f))
    return f


def figure_to_png(f):
    with io.BytesIO() as out:
        f.savefig(out, format='png', bbox_inches='tight')
        return out.getvalue()
