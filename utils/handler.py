import functools

from telethon.tl.types import Message

from reactor import Context


def magic_decorator(dec):
    @functools.wraps(dec)
    def _magic_decorator_wrapper(*args, **kwargs):
        if args and len(args) == 1 and callable(args[0]):
            return dec(args[0])
        elif not args and kwargs:
            def real_decorator(fn):
                return dec(fn, **kwargs)

            return real_decorator
        elif not args and not kwargs:
            return dec
        else:
            raise ValueError(f'Incorrect arguments for {dec.__name__}: {args} {kwargs}!')

    return _magic_decorator_wrapper


@magic_decorator
def respond_with_return(fn, **dec_kwargs):
    async def wrapper(message: Message, context: Context):
        response = await context.invoke(fn)
        if response is not None:
            await message.respond(response, **dec_kwargs)

    return wrapper


@magic_decorator
def respond_with_yield(fn, **dec_kwargs):
    async def wrapper(message: Message, context: Context):
        response = [line async for line in context.invoke(fn)]
        if response:
            await message.respond('\n'.join(response), **dec_kwargs)

    return wrapper
